package org.kosinskyi.dao;

import org.kosinskyi.db.DbConnection;
import org.kosinskyi.entity.PersistentCookie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;
import java.util.Optional;

public class PersistentCookieDAO implements DAO<PersistentCookie> {

	private DbConnection dbConnection;
	private static final Logger LOG = LoggerFactory.getLogger(PersistentCookieDAO.class);

	public PersistentCookieDAO(DbConnection dbConnection) {
		this.dbConnection = dbConnection;
	}

	@Override
	public Optional<PersistentCookie> get(String paramName, String paramValue) {
		Optional<PersistentCookie> result = Optional.empty();
		Connection connection = dbConnection.getConnection();
		String sqlStatement = String.format("select * from persistent_cookies where %s='%s'",paramName, paramValue);
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();
			while (resultSet.next()) {
				String persistentCookieValue = resultSet.getString("value");
				String persistentCookieUserLogin = resultSet.getString("user_login");
				Timestamp persistentCookieExpirationDate = resultSet.getTimestamp("expiration_date");
				result = Optional.of(new PersistentCookie(persistentCookieValue, persistentCookieUserLogin, persistentCookieExpirationDate));
			}
		} catch (SQLException e) {
			LOG.info(e.getMessage());
		}
		return result;
	}

	@Override
	public List<PersistentCookie> getAll() {
		return null;
	}

	@Override
	public void save(PersistentCookie persistentCookie) throws IllegalArgumentException {
		Connection connection = dbConnection.getConnection();
		String sqlStatement = "insert into persistent_cookies (value, user_login, expiration_date) values (?,?,?)";
		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);
			preparedStatement.setString(1, persistentCookie.getValue());
			preparedStatement.setString(2, persistentCookie.getUserLogin());
			preparedStatement.setTimestamp(3, persistentCookie.getExpirationTime());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			LOG.info(e.getMessage());
		}
	}

	@Override
	public void update(PersistentCookie persistentCookie, String[] params) {

	}

	@Override
	public void delete(PersistentCookie persistentCookie) {

	}
}
