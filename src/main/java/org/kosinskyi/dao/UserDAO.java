package org.kosinskyi.dao;

import org.kosinskyi.db.DbConnection;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.PassHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class UserDAO implements DAO <User> {

    private DbConnection dbConnection;
    private PassHash passHash;
    private static final Logger LOG = LoggerFactory.getLogger(UserDAO.class);

    public UserDAO(DbConnection dbConnection, PassHash passHash) {
        this.passHash = passHash;
        this.dbConnection = dbConnection;
    }

    @Override
    public Optional<User> get(String paramName, String paramValue) {
        Optional<User> result = Optional.empty();
        Connection connection = dbConnection.getConnection();
        String sqlStatement = String.format("select * from registered_users where %s='%s'",paramName, paramValue);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getResultSet();
            while (resultSet.next()) {
                int userId = resultSet.getInt("id");
                String userEmail = resultSet.getString("user_email");
                String userLogin = resultSet.getString("user_login");
                String userPass = resultSet.getString("user_pass");
                result = Optional.of(new User(userId, userEmail, userLogin, userPass));
            }
        } catch (SQLException e) {
            LOG.info(e.getMessage());
        }
        return result;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public void save(User user) throws IllegalArgumentException {
        Connection connection = dbConnection.getConnection();
        String sqlStatement = "insert into registered_users (user_login, user_email, user_pass) values (?,?,?)";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sqlStatement);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getEmail());
            preparedStatement.setString(3, user.getPassHash());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            LOG.info(e.getMessage());
            if (e.getMessage().contains("registered_user_user_login")) {
                throw new IllegalArgumentException("loginIsRegistered");
            } else if (e.getMessage().contains("registered_user_user_email")) {
                throw new IllegalArgumentException("emailIsRegistered");
            } else {
                throw new IllegalArgumentException("unknownError");
            }
        }
    }

    @Override
    public void update(User user, String[] params) {

    }

    @Override
    public void delete(User user) {

    }
}
