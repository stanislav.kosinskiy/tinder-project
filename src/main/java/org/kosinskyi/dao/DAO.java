package org.kosinskyi.dao;

import java.util.List;
import java.util.Optional;

public interface DAO <T> {

    Optional<T> get(String paramName, String paramValue);

    List<T> getAll();

    void save(T t) throws IllegalArgumentException;

    void update(T t, String[] params);

    void delete(T t);
}
