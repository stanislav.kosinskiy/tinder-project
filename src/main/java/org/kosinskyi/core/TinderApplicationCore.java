package org.kosinskyi.core;

import org.kosinskyi.dao.DAO;
import org.kosinskyi.db.DbConnection;
import org.kosinskyi.entity.PersistentCookie;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;
import org.kosinskyi.utils.PassHash;

public class TinderApplicationCore implements ApplicationCore {
    private DbConnection dbConnection;
    private FreeMarker freeMarker;
    private DAO<User> userDAO;
    private DAO<PersistentCookie> persistentCookieDAO;
    private PassHash passHash;

    public TinderApplicationCore(DbConnection dbConnection,
                                 FreeMarker freeMarker,
                                 DAO<User> userDAO,
                                 DAO<PersistentCookie> persistentCookieDAO,
                                 PassHash passHash) {
        this.dbConnection = dbConnection;
        this.freeMarker = freeMarker;
        this.userDAO = userDAO;
        this.passHash = passHash;
        this.persistentCookieDAO = persistentCookieDAO;
    }

    public DbConnection getDbConnection() {
        return dbConnection;
    }

    public FreeMarker getFreeMarker() {
        return freeMarker;
    }

    public DAO<User> getUserDAO() {
        return userDAO;
    }

    public PassHash getPassHash() {
        return passHash;
    }

    public DAO<PersistentCookie> getPersistentCookieDAO() {
        return persistentCookieDAO;
    }
}
