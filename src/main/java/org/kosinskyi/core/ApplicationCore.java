package org.kosinskyi.core;

import org.kosinskyi.dao.DAO;
import org.kosinskyi.db.DbConnection;
import org.kosinskyi.entity.PersistentCookie;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;
import org.kosinskyi.utils.PassHash;

public interface ApplicationCore {
    public DbConnection getDbConnection();
    public FreeMarker getFreeMarker();
    public DAO<User> getUserDAO();
    public PassHash getPassHash();
    public DAO<PersistentCookie> getPersistentCookieDAO();
}
