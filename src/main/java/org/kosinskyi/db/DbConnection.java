package org.kosinskyi.db;

import java.sql.Connection;
import java.sql.SQLException;

public interface DbConnection {
    public Connection getConnection();
}
