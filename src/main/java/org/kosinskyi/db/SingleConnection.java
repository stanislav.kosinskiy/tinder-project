package org.kosinskyi.db;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SingleConnection implements DbConnection{
    private final Properties dbProperties;
    private static Connection connection;

    public SingleConnection(Properties properties) throws IOException {
        this.dbProperties = properties;
    }

    public Connection getConnection() {

        if (connection == null) {
            try {
                Class.forName("org.postgresql.Driver");
                String dbUrl = dbProperties.getProperty("dburl");
                String dbLogin = dbProperties.getProperty("dblogin");
                String dbPass = dbProperties.getProperty("dbpass");
                connection = DriverManager.getConnection(dbUrl, dbLogin, dbPass);
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return connection;
    }

//    private Properties getDBProperties(File properties) throws IOException {
//        FileInputStream propertiesStream = new FileInputStream(properties);
//        Properties dbProperties = new Properties();
//        dbProperties.load(propertiesStream);
//        return dbProperties;
//    }
}
