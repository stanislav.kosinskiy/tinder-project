package org.kosinskyi.db;

import org.apache.commons.dbcp.BasicDataSource;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnectionPool implements DbConnection {

	private Properties dbProperties;
	private BasicDataSource ds = new BasicDataSource();

	public DBConnectionPool(Properties properties) throws IOException {
		dbProperties = properties;
		init();
	}



	private void init() {
		ds.setUrl(dbProperties.getProperty("dburl"));
		ds.setUsername(dbProperties.getProperty("dblogin"));
		ds.setPassword(dbProperties.getProperty("dbpass"));
		ds.setMinIdle(5);
		ds.setMaxIdle(10);
		ds.setMaxOpenPreparedStatements(100);
	}

	@Override
	public Connection getConnection() {
		try {
			return ds.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
