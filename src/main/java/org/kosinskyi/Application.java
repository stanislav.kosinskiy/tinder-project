package org.kosinskyi;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.session.SessionHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.*;
import org.kosinskyi.core.ApplicationCore;
import org.kosinskyi.core.TinderApplicationCore;
import org.kosinskyi.dao.DAO;
import org.kosinskyi.dao.PersistentCookieDAO;
import org.kosinskyi.dao.UserDAO;
import org.kosinskyi.db.DbConnection;
import org.kosinskyi.db.SingleConnection;
import org.kosinskyi.entity.PersistentCookie;
import org.kosinskyi.entity.User;
import org.kosinskyi.filters.WelcomeFilter;
import org.kosinskyi.servlets.*;
import org.kosinskyi.utils.FreeMarker;
import org.kosinskyi.utils.PassHash;

import javax.servlet.DispatcherType;
import java.util.EnumSet;
import java.util.Properties;

public class Application {

	//TODO check why DBCP is not working

	public static void main(String[] args) throws Exception {
		Server server = new Server(8080);

		Properties properties = new Properties();
		properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("DBProperties.properties"));
		DbConnection postgresConnection = new SingleConnection(properties);
//        DbConnection postgresConnection = new DBConnectionPool(properties);
		FreeMarker freeMarker = new FreeMarker(Application.class);
		PassHash passHash = new PassHash();
		DAO<User> userDAO = new UserDAO(postgresConnection, passHash);
		DAO<PersistentCookie> persistentCookieDAO = new PersistentCookieDAO(postgresConnection);
		ApplicationCore applicationCore = new TinderApplicationCore(
				postgresConnection,
				freeMarker,
				userDAO,
				persistentCookieDAO,
				passHash);

		FilterHolder filterHolder = new FilterHolder(CrossOriginFilter.class);
		filterHolder.setInitParameter("allowedOrigins", "*");
		filterHolder.setInitParameter("allowedMethods", "GET, POST");

		ServletContextHandler servletContextHandler = new ServletContextHandler();
		servletContextHandler.setSessionHandler(new SessionHandler());
		servletContextHandler.addServlet(new ServletHolder(new ServletLogin(applicationCore)), "/login");
		servletContextHandler.addServlet(new ServletHolder(new ServletRegister(applicationCore)), "/registration");
		servletContextHandler.addServlet(new ServletHolder(new ServletCabinet(applicationCore)), "/cabinet");
		servletContextHandler.addServlet(new ServletHolder(new ServletLogout(applicationCore)), "/logout");
		servletContextHandler.addServlet(new ServletHolder(new ServletForgotPassword(applicationCore)), "/forgot-password");

		servletContextHandler.addServlet(new ServletHolder(new ServletAssets(applicationCore)), "/assets/*");

		servletContextHandler.addFilter(WelcomeFilter.class, "/", EnumSet.of(DispatcherType.INCLUDE, DispatcherType.REQUEST));
		servletContextHandler.addFilter(filterHolder, "/*", null);

		server.setHandler(servletContextHandler);
		server.start();
		server.join();
	}
}
