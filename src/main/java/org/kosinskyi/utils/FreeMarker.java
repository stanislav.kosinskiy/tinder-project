package org.kosinskyi.utils;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public final class FreeMarker {
    /**
     * logger instance
     */
    private static final Logger LOG = LoggerFactory.getLogger(FreeMarker.class);

    /**
     * Configuration Instance
     */
    private final Configuration config;

    public FreeMarker(Class classLoader) {
        this.config = new Configuration(Configuration.VERSION_2_3_28) {{
            setClassLoaderForTemplateLoading(classLoader.getClassLoader(), "assets");
            setDefaultEncoding(String.valueOf(StandardCharsets.UTF_8));
            setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            setLogTemplateExceptions(false);
            setWrapUncheckedExceptions(true);
        }};
    }

    public void render(final String templateFile, final Map<String, Object> data, final HttpServletResponse resp) throws IOException {
        try {
            resp.setCharacterEncoding(String.valueOf(StandardCharsets.UTF_8));
            config.getTemplate(templateFile).process(data, resp.getWriter());
        } catch (TemplateException e) {
            LOG.info(e.getMessage());
        }
    }
    public void render(final String templateFile, final HttpServletResponse resp) throws IOException {
        HashMap<String, Object> data = new HashMap<>();
        render(templateFile, data, resp);
    }
}
