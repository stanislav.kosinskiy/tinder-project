package org.kosinskyi.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

public class PassHash {

	private static byte [] salt = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
	private static final Logger LOG = LoggerFactory.getLogger(PassHash.class);

	public String passHash(String password)  {
		try {
			KeySpec spec = new PBEKeySpec(password.toCharArray(), salt, 65536, 128);
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = factory.generateSecret(spec).getEncoded();
			return new String(hash);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			LOG.info(e.getMessage());
			return "";
		}
	}

//	private static byte[] getSalt() {
//		SecureRandom random = new SecureRandom();
//		byte[] salt = new byte[16];
//		random.nextBytes(salt);
//		return salt;
//	}
}
