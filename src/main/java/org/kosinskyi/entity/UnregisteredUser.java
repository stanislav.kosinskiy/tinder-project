package org.kosinskyi.entity;

public class UnregisteredUser extends User {
	public UnregisteredUser(String email, String login, String pass) {
		super(null, email, login, pass);
	}
}
