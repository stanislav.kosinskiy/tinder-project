package org.kosinskyi.entity;

import java.sql.Time;
import java.sql.Timestamp;

public class PersistentCookie {
	private String value;
	private String userLogin;
	private Timestamp expirationDate;

	public PersistentCookie(String value, String userLogin, Timestamp expirationDate) {
		this.value = value;
		this.userLogin = userLogin;
		this.expirationDate = expirationDate;
	}

	public String getValue() {
		return value;
	}

	public String getUserLogin() {
		return userLogin;
	}

	public Timestamp getExpirationTime() {
		return expirationDate;
	}
}
