package org.kosinskyi.entity;

public class User {
    private Integer id;
    private String email;
    private String login;
    private String passHash;

    public User(Integer id, String email, String login, String passHash) {
        this.id = id;
        this.email = email;
        this.login = login;
        this.passHash = passHash;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getLogin() {
        return login;
    }

    public String getPassHash() {
        return passHash;
    }

}
