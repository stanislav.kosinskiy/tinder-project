package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;
import org.kosinskyi.dao.DAO;
import org.kosinskyi.dao.UserDAO;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class ServletForgotPassword extends HttpServlet {

	private DAO<User> userDAO;
	private FreeMarker freeMarker;

	public ServletForgotPassword(ApplicationCore applicationCore) {
		this.freeMarker = applicationCore.getFreeMarker();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		freeMarker.render("/static/forgot-password.html", resp);
	}

	//TODO write implementation

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


	}


//	private Optional<User> getUserByEmail(HttpServletRequest req) {
//		return userDAO.get("email", getRequestParameter(req, "inputEmail"));
//	}

	private String getRequestParameter(HttpServletRequest req, String param) {
		return req.getParameterMap().get(param)[0];
	}
}
