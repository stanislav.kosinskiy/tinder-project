package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;
import org.kosinskyi.dao.DAO;
import org.kosinskyi.entity.PersistentCookie;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

public class ServletCabinet extends HttpServlet {

    private FreeMarker freeMarker;
    private DAO<PersistentCookie> persistentCookieDAO;
    private DAO<User> userDAO;

    public ServletCabinet(ApplicationCore applicationCore) {
        this.freeMarker = applicationCore.getFreeMarker();
        this.persistentCookieDAO = applicationCore.getPersistentCookieDAO();
        this.userDAO = applicationCore.getUserDAO();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        boolean isPersistentCookieinRequest = getPersistentCookie(cookies) != null;
        if (isPersistentCookieinRequest) {
            User user = getUserFromPersistentCookie(getPersistentCookie(cookies));
            req.getSession().setAttribute("user", user);
            HashMap<String, Object> data = new HashMap<>();
            data.put("user", user);
            freeMarker.render("static/cabinet.html", data, resp);
        } else if (req.getSession(false) == null) {
            resp.sendRedirect("/login");
        }  else {
            User user = (User) req.getSession().getAttribute("user");
            HashMap<String, Object> data = new HashMap<>();
            data.put("user", user);
            freeMarker.render("static/cabinet.html", data, resp);
        }
    }

    private Cookie getPersistentCookie(Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("PJSESSIONID")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    private User getUserFromPersistentCookie(Cookie persistentCookie) {
        PersistentCookie persistentCookieObject = persistentCookieDAO.get("value", persistentCookie.getValue()).get();
        String userLogin = persistentCookieObject.getUserLogin();
        return userDAO.get("user_login", userLogin).get();
    }

    private void getTournamentsList() throws IOException, SAXException, ParserConfigurationException {
        URL url = new URL("http://api.sportradar.us/basketball-t1/en/tournaments.xml?api_key=hapx3uuqnvy5hrmca2tr7ve8");
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//        if (urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
//            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
//            reader.lines().forEach(System.out::println);
//        }
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
//        Document doc = dBuilder.parse(new File());
//        NodeList tournaments = doc.getElementsByTagName("tournaments");
    }
}
