package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletLogout extends HttpServlet {

	public ServletLogout(ApplicationCore applicationCore) {
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().invalidate();
		Cookie persistentCookie = getPersistentCookie(req.getCookies());
		removePersistentCookie(persistentCookie, resp);
		resp.sendRedirect("/login");
	}

	private Cookie getPersistentCookie(Cookie[] cookies) {
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals("PJSESSIONID")) {
					return cookie;
				}
			}
		}
		return null;
	}

	private void removePersistentCookie(Cookie persistentCookie, HttpServletResponse resp) {
		if (persistentCookie!= null) {
			persistentCookie.setMaxAge(0);
			resp.addCookie(persistentCookie);
		}
	}
}
