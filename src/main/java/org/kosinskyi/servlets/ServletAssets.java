package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.IOUtils;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

public class ServletAssets extends HttpServlet {

    private static final String ASSETS_PATH = "assets";
    private static final Logger LOG = LoggerFactory.getLogger(ServletAssets.class);

    public ServletAssets(ApplicationCore applicationCore) {
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(ASSETS_PATH + req.getPathInfo());
        try {
            ServletOutputStream outputStream = resp.getOutputStream();
            if (req.getPathInfo().contains("css")) {
                resp.setContentType("text/css");
            }
            if (inputStream != null) {
                IOUtils.copy(inputStream, outputStream);
                inputStream.close();
                outputStream.close();
            }
        }
        catch (IOException e) {
            LOG.info(e.getMessage());
        }
    }
}
