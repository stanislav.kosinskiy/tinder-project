package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;
import org.kosinskyi.dao.DAO;
import org.kosinskyi.entity.UnregisteredUser;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;
import org.kosinskyi.utils.PassHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ServletRegister extends HttpServlet {

	private DAO <User> userDAO;
	private PassHash passHash;
	private FreeMarker freeMarker;
	private static HashMap <String, String> errorsDictionary = getErrorsDictionary();

	private static final Logger LOG = LoggerFactory.getLogger(ServletRegister.class);

	public ServletRegister(ApplicationCore applicationCore) {
		this.userDAO = applicationCore.getUserDAO();
		this.passHash = applicationCore.getPassHash();
		this.freeMarker = applicationCore.getFreeMarker();
	}

	private static HashMap<String, String> getErrorsDictionary() {
		HashMap<String, String> errors = new HashMap<>();
		errors.put("usernameIsNotValid", "Username may consist only of latin letters and digits");
		errors.put("loginIsRegistered", "User with current login already registered");
		errors.put("emailIsRegistered", "User with current email already registered");
		errors.put("passIsNotValid", "Pass may consist only of latin letters and digits and should be longer then 8 symbols");
		errors.put("passIsNotConfirmed", "Pass and confirmation pass doesn't match");
		return errors;
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		HashMap<String, Object> data = new HashMap<>();
		data.put("isErrorInRequest", isErrorInRequest(req));
		data.put("error", errorsDictionary.getOrDefault(getAuthError(req), "unknown error"));
		freeMarker.render("static/registration.html", data, resp);
	}

	private boolean isErrorInRequest(HttpServletRequest req) {
		return req.getParameterMap().containsKey("error");
	}

	private String getAuthError(HttpServletRequest req) {
		return req.getParameter("error");
	}

	@SuppressWarnings("Duplicates")
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String username = req.getParameter("username");
		String email = req.getParameter("email");
		String passwordUnhashed = req.getParameter("password");
		String password = passHash.passHash(req.getParameter("password"));
		String confirmPassword = passHash.passHash(req.getParameter("confirmPassword"));
		String registrationDataError = getRegistrationDataError(username, email, password, confirmPassword, passwordUnhashed);
		boolean isRegistrationDataValid = processRegistrationDataError(registrationDataError, resp);
		if (isRegistrationDataValid) {
			registerUser(email, username, password, resp);
			freeMarker.render("/static/registrationSuccessful.html", resp);
		}
	}

	private String getRegistrationDataError(String username, String email, String password, String confirmPassword, String passwordUnhashed) {
		String registrationDataError = null;
		boolean isUsernameValid = username.matches("[A-Za-z0-9]+");
		boolean isPassValid = passwordUnhashed.matches("[A-Za-z0-9]+") && passwordUnhashed.length() >= 8;
		boolean isPassMatches = password.equals(confirmPassword);
		if (!isUsernameValid) {
			registrationDataError = "usernameIsNotValid";
		}
		if (!isPassValid) {
			registrationDataError = "passIsNotValid";
		}
		if (!isPassMatches) {
			registrationDataError = "passIsNotConfirmed";
		}
		return registrationDataError;
	}

	private boolean processRegistrationDataError(String registrationDataError, HttpServletResponse resp) throws IOException {
		if (registrationDataError != null) {
			resp.sendRedirect("/registration?error="+registrationDataError);
			return false;
		} else {
			return true;
		}
	}

	private void registerUser(String email, String username, String password, HttpServletResponse resp) throws IOException {
		try {
			userDAO.save(new UnregisteredUser(email, username, password));
		} catch (IllegalArgumentException e) {
			resp.sendRedirect("/registration?error="+e.getMessage());
		}
	}
}
