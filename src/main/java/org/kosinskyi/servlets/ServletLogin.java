package org.kosinskyi.servlets;

import org.kosinskyi.core.ApplicationCore;
import org.kosinskyi.dao.DAO;
import org.kosinskyi.entity.PersistentCookie;
import org.kosinskyi.entity.User;
import org.kosinskyi.utils.FreeMarker;
import org.kosinskyi.utils.PassHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

public class ServletLogin extends HttpServlet {

    private DAO<User> userDAO;
    private PassHash passHash;
    private FreeMarker freeMarker;
    private DAO<PersistentCookie> persistentCookieDAO;
    private static final int SESSION_MAX_INACTIVE_INTERBAL = 60*20;
    private static final int PERSISTENT_COOKIE_MAX_AGE = 60*60*24*7;

    private static final Logger LOG = LoggerFactory.getLogger(ServletLogin.class);

    public ServletLogin(ApplicationCore applicationCore) {
        this.userDAO = applicationCore.getUserDAO();
        this.passHash = applicationCore.getPassHash();
        this.freeMarker = applicationCore.getFreeMarker();
        this.persistentCookieDAO = applicationCore.getPersistentCookieDAO();
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isPersistentCookieInRequest = getPersistentCookie(req.getCookies()) != null;
        if (isPersistentCookieInRequest || req.getSession(false) != null) {
            resp.sendRedirect("/cabinet");
        } else {
            HashMap<String, Object> data = new HashMap<>();
            data.put("isErrorInRequest", isErrorInRequest(req));
            freeMarker.render("static/login.html", data, resp);
        }
    }

    // helper methods for doGet

    private boolean isErrorInRequest(HttpServletRequest req) {
        return req.getParameterMap().containsKey("error");
    }

    private Cookie getPersistentCookie(Cookie[] cookies) {
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("PJSESSIONID")) {
                    return cookie;
                }
            }
        }
        return null;
    }

    @SuppressWarnings({"OptionalGetWithoutIsPresent", "Duplicates"})
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        boolean isCheckedRememberMe = req.getParameter("rememberMe") != null;
        if (areCredentialsCorrect(req)) {
            User user = getUserByLogin(req).get();
            addUserToSession(user, req);
            savePersistentCookie(isCheckedRememberMe, user, resp);
            resp.sendRedirect("/cabinet");
        } else {
            resp.sendRedirect("/login?error=invalidCredentials");
        }
    }

    // helper methods for doPost

    private boolean areCredentialsCorrect(HttpServletRequest req) {
        Optional <User> optionalUser = getUserByLogin(req);
        String userPass = passHash.passHash(req.getParameter( "inputPassword"));
        return optionalUser.isPresent() && optionalUser.get().getPassHash().equals(userPass);
    }

    private Optional<User> getUserByLogin(HttpServletRequest req) {
        return userDAO.get("user_login", req.getParameter( "inputUsername"));
    }

    private void addUserToSession(User user, HttpServletRequest req) {
        HttpSession session = req.getSession(true);
        session.setMaxInactiveInterval(SESSION_MAX_INACTIVE_INTERBAL);
        session.setAttribute("user", user);
    }

    private void savePersistentCookie(boolean isCheckedRememberMe, User user, HttpServletResponse resp) {
        if (isCheckedRememberMe) {
            Cookie cookie = new Cookie("PJSESSIONID", UUID.randomUUID().toString());
            cookie.setMaxAge(PERSISTENT_COOKIE_MAX_AGE);
            resp.addCookie(cookie);
            Timestamp expirationTime = new Timestamp(System.currentTimeMillis() + 1000*60*60*24*7);
            persistentCookieDAO.save(new PersistentCookie(cookie.getValue(), user.getLogin(), expirationTime));
        }
    }
}
