package org.kosinskyi.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class WelcomeFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		HttpServletRequest request = (HttpServletRequest) servletRequest;

		ArrayList<String> strings = new ArrayList<>();
		strings.add("/login");
		strings.add("/register");
		strings.add("/cabinet");

		if (!strings.contains(request.getPathInfo())) {
			response.sendRedirect("/login");
		}
		filterChain.doFilter(servletRequest, response);
	}

	@Override
	public void destroy() {

	}
}
